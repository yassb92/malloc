#include "alignment.h"

size_t align(size_t size)
{
    int ret;
    size_t len = 0;
    size_t temp = 0;
    ret = __builtin_add_overflow(size, sizeof(long double), &temp);
    if (ret)
        return 0;
    while (size > len)
    {
        ret = __builtin_add_overflow(len, sizeof(long double), &len);
        if (ret)
            return 0;
    }
    return len;
}
