#include "recycler.h"

#include <stddef.h>
#include <stdlib.h>

struct recycler *recycler_create(size_t block_size, size_t total_size)
{
    if (block_size == 0 || total_size == 0)
        return NULL;
    if ((block_size % sizeof(size_t)) != 0)
        return NULL;
    if ((total_size % block_size) != 0)
        return NULL;
    struct recycler *r = malloc(sizeof(struct recycler));
    if (!r)
        return NULL;
    r->block_size = block_size;
    r->capacity = total_size / block_size;
    r->chunk = malloc(total_size);
    if (!(r->chunk))
    {
        free(r);
        return NULL;
    }
    r->free = r->chunk;
    struct free_list *list = r->free;
    for (size_t i = 0; i < r->capacity - 1; i++)
    {
        char *temp = (char *)list + block_size;
        void *res = temp;
        list->next = res;
        list = list->next;
    }
    return r;
}

void recycler_destroy(struct recycler *r)
{
    if (!r)
        return;
    free(r->chunk);
    free(r);
}

void *recycler_allocate(struct recycler *r)
{
    if (!r)
        return NULL;
    void *block;
    if (r->free)
    {
        block = r->free;
        struct free_list *tmp = r->free;
        r->free = tmp->next;
        return block;
    }
    else
        return NULL;
}

void recycler_free(struct recycler *r, void *block)
{
    if (!r || !block)
        return;
    void *temp = r->free;
    r->free = block;
    struct free_list *l = block;
    l->next = temp;
}
