#include "beware_overflow.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

void *beware_overflow(void *ptr, size_t nmemb, size_t size)
{
    char *temp;
    size_t res = 0;
    int ret = __builtin_mul_overflow(nmemb, size, &res);
    if (!ret)
    {
        temp = ptr;
        temp += res;
        ptr = temp;
        return ptr;
    }
    else
        return NULL;
}
