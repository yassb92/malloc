#include "allocator.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>

struct blk_allocator *blka_new(void)
{
    struct blk_allocator *new = malloc(sizeof(struct blk_allocator));
    if (!new)
        return NULL;
    new->meta = NULL;
    return new;
}

struct blk_meta *blka_alloc(struct blk_allocator *blka, size_t size)
{
    struct blk_meta *new;
    size_t blksize = 0;
    if (size == 0)
    {
        new = mmap(NULL, sysconf(_SC_PAGESIZE), PROT_READ | PROT_WRITE,
                   MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
        if (new == MAP_FAILED)
            return NULL;
        new->size = sysconf(_SC_PAGESIZE) - sizeof(struct blk_meta);
    }
    else
    {
        blksize = size;
        while (blksize % sysconf(_SC_PAGESIZE) != 0)
            blksize++;
        new = mmap(NULL, blksize, PROT_READ | PROT_WRITE,
                   MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
        if (new == MAP_FAILED)
            return NULL;
        new->size = blksize - sizeof(struct blk_meta);
    }
    struct blk_meta *temp = blka->meta;
    blka->meta = new;
    new->next = temp;
    return new;
}

void blka_free(struct blk_meta *blk)
{
    munmap(blk, blk->size + sizeof(struct blk_meta));
    return;
}

void blka_pop(struct blk_allocator *blka)
{
    if (!blka->meta)
        return;
    struct blk_meta *tmp = blka->meta->next;
    blka_free(blka->meta);
    blka->meta = tmp;
}

void blka_delete(struct blk_allocator *blka)
{
    while (blka->meta)
        blka_pop(blka);
    free(blka);
}
