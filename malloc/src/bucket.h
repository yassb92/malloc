#ifndef BUCKET_H
#define BUCKET_H

#define _GNU_SOURCE

#include <stddef.h>

struct bucket
{
    size_t block_size;
    size_t capacity; // number of blocks in the chunk
    void *chunk; // memory chunk containing all blocks
    void *free; // address of the first free block of the free list
    struct bucket *next;
};

struct free_list
{
    struct free_list *next; // next free block
};

struct bucket *bucket_create(size_t block_size);
void bucket_destroy(struct bucket *b);
void *bucket_allocate(struct bucket *b);
void bucket_free(struct bucket *b, void *block);
struct bucket *find_bucket(struct bucket *b, size_t n);
void print_bucket(struct bucket *b);
int is_empty(struct bucket *b);
struct bucket *append_bucket(struct bucket b, struct bucket *new);

#endif /*!BUCKET_H*/
