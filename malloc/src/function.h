#ifndef FUNCTION_H
#define FUNCTION_H

#define _GNU_SOURCE

#include <stddef.h>

#include "bucket.h"

size_t align(size_t size);

size_t align2(size_t size);
size_t next_pow_two(size_t n);
size_t align_pagesize(size_t n);
int is_empty(struct bucket *b);

#endif /*!FUNCTION_H*/
