#include "function.h"

#include <stddef.h>
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>

#include "bucket.h"

size_t next_pow_two(size_t n)
{
    size_t res = 1;
    while (res < n)
        res = res * 2;
    return res;
}

size_t align_pagesize(size_t n)
{
    size_t res = 1;

    while (res < n || res < (size_t)sysconf(_SC_PAGESIZE))
    {
        // printf("res = %ld\n", res);
        res = res * 2;
    }

    // printf("finale = %ld\n", res);
    return res;
}

size_t align2(size_t size)
{
    int ret;
    size_t len = 0;
    size_t temp = 0;
    ret = __builtin_add_overflow(size, 2, &temp);
    if (ret)
        return 0;
    while (size > len)
    {
        ret = __builtin_add_overflow(len, 2, &len);
        if (ret)
            return 0;
    }
    return len;
}
size_t align(size_t size)
{
    int ret;
    size_t len = 0;
    size_t temp = 0;
    ret = __builtin_add_overflow(size, sizeof(long double), &temp);
    if (ret)
        return 0;
    while (size > len)
    {
        ret = __builtin_add_overflow(len, sizeof(long double), &len);
        if (ret)
            return 0;
    }
    return len;
}

int is_empty(struct bucket *b)
{
    struct free_list *cursor = b->free;
    size_t compteur = 0;
    if (!cursor)
        return 0;
    while (cursor)
    {
        compteur++;
        cursor = cursor->next;
    }
    if (compteur == b->capacity)
        return 1;
    else
        return 0;
}
void print_bucket(struct bucket *b)
{
    printf("BUCKET: ");
    struct bucket *cursor = b;
    while (cursor)
    {
        printf("address = %p\n", cursor);
        printf("size = %d, ", cursor->block_size);
        printf("capacity = %d, ", cursor->capacity);
        printf("address chunk = %p\n", cursor->chunk);
        printf("address next = %p\n", cursor->next);
        cursor = cursor->next;
    }
    printf("\n");
}
