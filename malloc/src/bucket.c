#include "bucket.h"

#include <stddef.h>
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>

#include "function.h"

struct bucket *bucket_create(size_t block_size)
{
    size_t size = align2(align(block_size));
    size_t page_size =
        align_pagesize(size) + align_pagesize(sizeof(struct bucket));
    struct bucket *b = mmap(NULL, page_size, PROT_READ | PROT_WRITE,
                            MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (b == MAP_FAILED)
        return NULL;
    b->block_size = size;
    b->capacity = align_pagesize(size) / size;
    void *ptr = b;
    char *temp = ptr;
    temp += align(sizeof(struct bucket));
    ptr = temp;
    b->chunk = ptr;
    b->next = NULL;
    b->free = ptr;
    struct free_list *list = b->free;
    for (size_t i = 0; i < b->capacity - 1; i++)
    {
        char *temp = (char *)list + size;
        void *res = temp;
        list->next = res;
        list = list->next;
    }
    list->next = NULL;
    return b;
}

void bucket_destroy(struct bucket *b)
{
    if (!b)
        return;
    size_t size =
        align_pagesize(b->block_size) + align_pagesize(sizeof(struct bucket));
    munmap(b, size);
}

void *bucket_allocate(struct bucket *b)
{
    if (!b)
        return NULL;
    void *block;
    if (b->free)
    {
        block = b->free;
        struct free_list *tmp = b->free;
        b->free = tmp->next;
        return block;
    }
    else
        return NULL;
}

void bucket_free(struct bucket *b, void *block)
{
    if (!b || !block)
        return;
    void *temp = b->free;
    b->free = block;
    struct free_list *l = block;
    l->next = temp;
}

struct bucket *find_bucket(struct bucket *b, size_t n)
{
    struct bucket *cursor = b;
    size_t size = next_pow_two(n);
    while (cursor)
    {
        if (cursor->block_size == size && cursor->free)
            return cursor;
        cursor = cursor->next;
    }
    return NULL;
}

struct bucket *append_bucket(struct bucket new, struct bucket *b)
{
    if (b == NULL)
    {
        struct bucket *b = &new;
        return b;
    }
    struct bucket *cursor = b;
    while (cursor->next)
        cursor = cursor->next;
    cursor->next = &new;
    return b;
}

/*void print_bucket(struct bucket *b)
{
    printf("BUCKET: ");
    struct bucket *cursor = b;
    while (cursor)
    {
        printf("address = %p\n", cursor);
        printf("size = %d, ", cursor->block_size);
        printf("capacity = %d, ", cursor->capacity);
        printf("address chunk = %p\n", cursor->chunk);
        printf("address next = %p\n", cursor->next);
        cursor = cursor->next;
    }
    printf("\n");
}*/
