#include "mymalloc.h"

#include <stddef.h>
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>

#include "bucket.h"

static struct bucket *bucket = NULL;

void *mymalloc(size_t size)
{
    if (bucket == NULL)
    {
        bucket = bucket_create(size);
        void *res = bucket_allocate(bucket);
        return res;
    }
    struct bucket *p = bucket;
    struct bucket *temp = find_bucket(p, size);
    if (temp == NULL)
    {
        struct bucket *b = bucket_create(size);
        void *res = bucket_allocate(b);
        b->next = bucket;
        bucket = b;
        return res;
    }
    void *res = bucket_allocate(temp);
    return res;
}
void *mycalloc(size_t number, size_t size)
{
    size_t s = 0;
    int ret = __builtin_mul_overflow(number, size, &s);
    if (!ret)
    {
        void *ptr = mymalloc(s);
        char *tmp = ptr;
        for (size_t i = 0; i < s; i++)
            tmp[i] = 0;
        return ptr;
    }
    else
        return NULL;
}

void *myrealloc(void *ptr, size_t size)
{
    if (ptr == NULL)
        return mymalloc(size);
    else if (size == 0 && ptr != NULL)
    {
        myfree(ptr);
        return NULL;
    }
    struct bucket *cursor = bucket;
    while (cursor && cursor->block_size < size)
        cursor = cursor->next;
    if (cursor == NULL || cursor->free == NULL)
    {
        struct bucket *new = bucket_create(size);
        void *res = bucket_allocate(new);
        new->next = bucket;
        bucket = new;
        char *c = res;
        char *tmp = ptr;
        for (size_t i = 0; i < size; i++)
            c[i] = tmp[i];
        return res;
    }
    void *res = bucket_allocate(cursor);
    char *c = res;
    char *tmp = ptr;
    for (size_t i = 0; i < size; i++)
        c[i] = tmp[i];
    return res;
}
void myfree(void *ptr)
{
    if (ptr == NULL)
    {
        return;
    }
    struct bucket *cursor = bucket;
    if (bucket->next == NULL)
    {
        bucket_free(bucket, ptr);
        if (is_empty(bucket))
        {
            bucket_destroy(bucket);
            bucket = NULL;
        }
        return;
    }
    while (cursor)
    {
        struct free_list *index = cursor->chunk;
        size_t i = 0;
        while (i < cursor->capacity)
        {
            if (index == ptr)
            {
                bucket_free(cursor, ptr);
                return;
            }
            index += cursor->block_size;
            i += 16;
        }
        cursor = cursor->next;
    }
}
