#include <criterion/criterion.h>

#include "mymalloc.h"

Test(Basic, Malloc)
{
    void *ptr = mymalloc(10);
    cr_assert_not_null(ptr);
}
