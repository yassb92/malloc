#include "page_begin.h"

#include <stddef.h>
#include <stdio.h>

void *page_begin(void *ptr, size_t page_size)
{
    size_t temp = (size_t)ptr;
    char *p = ptr;
    temp = temp - (temp & ((~(page_size - 1))));
    p -= temp;
    return p;
}
